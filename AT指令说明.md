

#### AT指令说明
- Debug		添加一些研发调试的指令
	- AT+Debug=cfg,reset		擦除FLASH存储的数据，重启后会重新载入默认数据

- Log
	- AT+Log?			查询日志等级
		- AT+Log#level	[level=QUIET/PANIC/FATAL/ERR/WARN/INFO/DEBUG]
	- AT+Log=level		配置日志等级
		- AT+Log#OK
	- AT+Log=pid,sta	配置pid日志开关	[sta=on/off]
		- AT+Log#OK
	- AT+Log=pid,sta,val	配置pid日志开关和频率	[sta=on/off] [val：单位毫秒]
		- AT+Log#OK
	- AT+Log=sel,sta	配置sel日志开关	[sta=on/off]
		- AT+Log#OK
	- AT+Log=sel,sta,val	配置sel日志开关和打印周期	[sta=on/off] [val：单位毫秒]
		- AT+Log#OK
	
- Reboot
	- AT+Reboot		复位芯片
		- AT+Reboot#OK
	
- Version
	- AT+Version?McuSoftware	查询软件版本
		- AT+Version#McuSoftware:1.0.0
		
- ColorWheel
	- AT+ColorWheel?Vsync			查询色轮同步信号速度
		- AT+ColorWheel#Vsync:val	[val：转速值]
	- AT+ColorWheel?Speed			查询色轮转速
		- AT+ColorWheel#Speed:val	[val：转速值]
	- AT+ColorWheel?Sync			查询色轮是否同步成功
		- AT+ColorWheel#Sync:val	[val：1=已同步，0=未同步]
	- AT+ColorWheel?PhaseErr			查询色轮相位差
		- AT+ColorWheel#PhaseErr:val	[val：同步信号与转速信号的相位差值]
		
- ColorWheelDelay
	- AT+ColorWheelDelay?			查询色轮延迟
		- AT+ColorWheelDelay#val	[val：0~359延迟角度]
	- AT+ColorWheelDelay=val		配置色轮延迟（不保存到FLASH）
		- AT+ColorWheelDelay#val	[val：0~359延迟角度]
		
- LightSource
	- AT+LightSource?				查询是否允许开关灯
		- AT+LightSource#sta		[sta：ON/OFF]
	- AT+LightSource=sta			用户配置允许开关灯	[sta：1/on=打开，0/off=关闭]
		- AT+LightSource#OK

- Pid
	- AT+PID=sta		开关PID控制PWM输出（默认开启）	[sta：1=打开，0=关闭]
		- AT+PID#OK	
	- AT+PID=PWM,0.5	配置PWM输出指定占空比（需要先关闭PID控制PWM输出功能）
		- AT+PID#OK	
	- AT+PID?Speed							查询PID控速度参数
		- AT+PID#speed,10,1.0e-1,1,0,0,0	
	- AT+PID=speed,10,1.0e-1,1,0,0,0		配置PID控速度参数
		- AT+PID#OK	
	- AT+PID?Phase							查询PID控相位参数
		- AT+PID#phase,10,1.0e-1,1,7e-1,5e-3,0
	- AT+PID=phase,10,1.0e-1,1,7e-1,5e-3,0	配置PID控相位参数
		- AT+PID#OK	
	
- Eeprom
	- AT+Eeprom?val					查询色轮延迟
		- AT+Eeprom#val:n				[n：0~359延迟角度]
	- AT+Eeprom=val:50				配置色轮延迟（会存储到FLASH，兼容MU10H指令集）
		- AT+Eeprom#OK
	- AT+Eeprom?ColorWheelDelay		查询色轮延迟
		- AT+Eeprom#ColorWheelDelay:n	[n：0~359延迟角度]
	- AT+Eeprom=ColorWheelDelay:50	配置色轮延迟
		- AT+Eeprom#OK
	
- LightMode				配置色轮同步异常时的关灯逻辑
	- AT+LightMode?		查询关灯逻辑
		- AT+LightMode#Phase:n		[Phase：相位模式，相位差超出n时认为需要关灯；n：相位差n以内认为同步成功，允许开灯]
		- AT+LightMode#Speed:n		[Phase：转速模式，转速低于n时认为需要关灯；n：转速RPM]
	- AT+LightMode=Speed,14000		配置关灯逻辑为转速低于14000转时关灯
		- AT+LightMode#OK
	- AT+LightMode=Phase,200		配置关灯逻辑为相位差超过200时关灯
		- AT+LightMode#OK

