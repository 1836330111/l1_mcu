/*
 * @File: i2c.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.I2C drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:14:16
 * @Copyright: Copyright (c) 2020
 */
#include "i2c.h"

as_iic_data iic_data[IIC_NUM_MAX];
static iic_rx_cb_t iic_rx_cb[IIC_NUM_MAX] = {0};


// Generic API
void HAL_I2C_SetRx_Callback(ae_iic_num num, iic_rx_cb_t cb)
{
    iic_rx_cb[num] = cb;
}

void HAL_I2C_SetTx(ae_iic_num num, uint8_t *buf, uint16_t len)
{
    if( len >= I2C_TX_BUFF_LEN ){
        len = I2C_TX_BUFF_LEN;
    }
    memset( &iic_data[num].tx, 0, sizeof(iic_data[num].tx) );
    memcpy( iic_data[num].tx.buff, buf, len );
    iic_data[num].tx.len = len;
}

void HAL_I2C_SendEvt(ae_iic_num num)
{
    if( iic_data[num].tx.r_idx < iic_data[num].tx.len ){
        switch(num){
            case IIC_NUM0:
                i2c_data_transmit(I2C0, iic_data[num].tx.buff[iic_data[num].tx.r_idx]);
                break;
            case IIC_NUM1:
                i2c_data_transmit(I2C1, iic_data[num].tx.buff[iic_data[num].tx.r_idx]);
                break;
        }
        iic_data[num].tx.r_idx ++;
    }else{
        switch(num){
            case IIC_NUM0:
                i2c_data_transmit(I2C0, 0xFF);
                break;
            case IIC_NUM1:
                i2c_data_transmit(I2C1, 0xFF);
                break;
        }
    }
}


// I2C0
void HAL_I2C0_Init(void)    // Slave
{
    memset(&iic_data[IIC_NUM0], 0, sizeof(iic_data[IIC_NUM0]));
    
    rcu_periph_clock_enable(RCU_I2C0);
    gpio_init(GPIOB, GPIO_MODE_AF_OD, GPIO_OSPEED_50MHZ, GPIO_PIN_6 | GPIO_PIN_7);

    i2c_software_reset_config(I2C0, I2C_SRESET_SET);
    i2c_software_reset_config(I2C0, I2C_SRESET_RESET);

    i2c_clock_config(I2C0, 100000, I2C_DTCY_2);

    i2c_mode_addr_config(I2C0, I2C_I2CMODE_ENABLE, I2C_ADDFORMAT_7BITS, HAL_I2C0_ADDR);

    i2c_enable(I2C0);

    i2c_ack_config(I2C0, I2C_ACK_ENABLE);

    i2c_interrupt_enable(I2C0, I2C_INT_EV);
    i2c_interrupt_enable(I2C0, I2C_INT_BUF);
    
    nvic_irq_enable(I2C0_EV_IRQn, 2, 2);
}

void I2C0_EV_IRQHandler(void)
{
    uint8_t tmp;

    if (i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_ADDSEND))
    {
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_ADDSEND);
        HAL_I2C_SendEvt(IIC_NUM0);
    }
    else if (i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_RBNE))
    {
        tmp = i2c_data_receive(I2C0);
        if( iic_rx_cb[IIC_NUM0] != NULL ){
            iic_rx_cb[IIC_NUM0](tmp);
        }
    }
    else if (i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_TBE))
    {
        HAL_I2C_SendEvt(IIC_NUM0);
    }
    else if (i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_STPDET))
    {
        i2c_stop_on_bus(I2C0);
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_TBE);
    }
    else if (i2c_interrupt_flag_get(I2C0, I2C_INT_FLAG_AERR))
    {
        i2c_interrupt_flag_clear(I2C0, I2C_INT_FLAG_AERR);
    }
}


// I2C soft
static int HAL_SOFT_I2C_Scl_0(ae_iic_num num)
{
    gpio_bit_write(iic_data[num].scl_port, iic_data[num].scl_pin, RESET);
    return 0;
}
static int HAL_SOFT_I2C_Scl_1(ae_iic_num num)
{
    gpio_bit_write(iic_data[num].scl_port, iic_data[num].scl_pin, SET);
    return 0;
}
static int HAL_SOFT_I2C_Sda_0(ae_iic_num num)
{
    gpio_bit_write(iic_data[num].sda_port, iic_data[num].sda_pin, RESET);
    return 0;
}
static int HAL_SOFT_I2C_Sda_1(ae_iic_num num)
{
    gpio_bit_write(iic_data[num].sda_port, iic_data[num].sda_pin, SET);
    return 0;
}
static int HAL_SOFT_I2C_Sda_Read(ae_iic_num num)
{
    return (gpio_input_bit_get(iic_data[num].sda_port, iic_data[num].sda_pin)==SET?1:0);
}
static void HAL_SOFT_I2C_Delay(ae_iic_num num)
{
    HAL_Delay_us(HAL_SOFT_I2C_DELAY);
}
static int HAL_SOFT_I2C_Start(ae_iic_num num)
{
    as_iic_api *thiz = &iic_data[num].api;
    thiz->sda_1(num);
    thiz->delay(num);
    thiz->scl_1(num);
    thiz->delay(num);
    thiz->sda_0(num);
    thiz->delay(num);
    thiz->scl_0(num);
    thiz->delay(num);
    return 0;
}
static int HAL_SOFT_I2C_Stop(ae_iic_num num)
{
    as_iic_api *thiz = &iic_data[num].api;
    thiz->scl_0(num);
    thiz->delay(num);
    thiz->sda_0(num);
    thiz->delay(num);
    thiz->scl_1(num);
    thiz->delay(num);
    thiz->sda_1(num);
    thiz->delay(num);
    return 0;
}
static int HAL_SOFT_I2C_Ack(ae_iic_num num)
{
    as_iic_api *thiz = &iic_data[num].api;
    thiz->scl_0(num);
    thiz->delay(num);
    thiz->sda_0(num);
    thiz->delay(num);
    thiz->scl_1(num);
    thiz->delay(num);
    thiz->scl_0(num);
    thiz->delay(num);
    return 0;
}
static int HAL_SOFT_I2C_Nack(ae_iic_num num)
{
    as_iic_api *thiz = &iic_data[num].api;
    thiz->scl_0(num);
    thiz->delay(num);
    thiz->sda_1(num);
    thiz->delay(num);
    thiz->scl_1(num);
    thiz->delay(num);
    thiz->scl_0(num);
    thiz->delay(num);
    return 0;
}
static int HAL_SOFT_I2C_WaitAck(ae_iic_num num)
{
    int ack;
    as_iic_api *thiz = &iic_data[num].api;

    thiz->scl_0(num);
    thiz->delay(num);
    gpio_init(iic_data[num].sda_port, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, iic_data[num].sda_pin);

    thiz->scl_1(num);
    thiz->delay(num);
    ack = (int)thiz->sda_read(num);
    thiz->scl_0(num);  
    thiz->delay(num);
    gpio_init(iic_data[num].sda_port, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, iic_data[num].sda_pin);
    
    return ack;  
}
static int HAL_SOFT_I2C_SendByte(ae_iic_num num, uint8_t byte)
{
    uint8_t i = 8;
    as_iic_api *thiz = &iic_data[num].api;
    
    while(i--)
    {
        thiz->scl_0(num); 
        thiz->delay(num);

        if (byte & 0x80){
            thiz->sda_1(num);  
        }
        else{
            thiz->sda_0(num);   
        }

        byte <<= 1;
        thiz->delay(num);
        thiz->scl_1(num);
        thiz->delay(num);
    }
    return 0;
}
static uint8_t HAL_SOFT_I2C_ReadByte(ae_iic_num num)
{
    uint8_t i = 8;
    uint8_t byte = 0;
    as_iic_api *thiz = &iic_data[num].api;

    thiz->sda_1(num);
    gpio_init(iic_data[num].sda_port, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, iic_data[num].sda_pin);
    while(i--)
    {
        byte <<= 1;

        thiz->scl_0(num);
        thiz->delay(num);

        thiz->scl_1(num);
        thiz->delay(num);
        if(thiz->sda_read(num))
            byte |= 0x01;
    }
    thiz->scl_0(num);
    gpio_init(iic_data[num].sda_port, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, iic_data[num].sda_pin);
    return byte;
}
static int HAL_SOFT_I2C_Reset(ae_iic_num num)
{
    as_iic_api *thiz = &iic_data[num].api;

    thiz->stop(num);   // stop
    return 0;
}

int HAL_SOFT_I2C_Transmit(ae_iic_num num, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
    as_iic_api *thiz = &iic_data[num].api;

    // thiz->init(num);
    // start
    if(thiz->start(num))
        goto ERR_i2c_gpio_transmit;

    // DeviceAddress
    thiz->send_byte(num, DevAddress & 0xFE);
    if(thiz->wait_ack(num)){
        thiz->stop(num); 
        goto ERR_i2c_gpio_transmit;
    }

    // data
    while(Size--)
    {
        thiz->send_byte(num, *pData);
        thiz->wait_ack(num);
        pData++;
    }

    // stop
    thiz->stop(num);
    return 0;

ERR_i2c_gpio_transmit:
    return 1;
}
int HAL_SOFT_I2C_Receive(ae_iic_num num, uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
    as_iic_api *thiz = &iic_data[num].api;

    // thiz->init(num);
    // start
    if(thiz->start(num))
        goto ERR_i2c_gpio_receive;

    // DeviceAddress Write
    thiz->send_byte(num, DevAddress | 0x01);
    if(thiz->wait_ack(num)){
        thiz->stop(num); 
        goto ERR_i2c_gpio_receive;
    }

    // data
    while(Size){
        *pData = thiz->read_byte(num);
        if (Size == 1)
            thiz->nack(num);
        else 
            thiz->ack(num);
        pData++;
        Size--;
    }

    // stop
    thiz->stop(num);
    return 0;

ERR_i2c_gpio_receive:
    return 1;
}
int HAL_SOFT_I2C_MemWrite(ae_iic_num num, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{
    int ret = 0;
    as_iic_api *thiz = &iic_data[num].api;

    // thiz->init(num);
    thiz->stop(num);
    // start
    if(thiz->start(num)){
        ret = -1;
        goto ERR_i2c_gpio_mem_write;
    }

    // DeviceAddress
    thiz->send_byte(num, DevAddress & 0xFE);
    if(thiz->wait_ack(num)){
        thiz->stop(num); 
        ret = -2;
        goto ERR_i2c_gpio_mem_write;
    }
    
    // MemAddress
    if (I2C_ADDR_SIZE_8BIT == MemAddSize){
        thiz->send_byte(num, MemAddress & 0xFF);
        if(thiz->wait_ack(num)){
            thiz->stop(num); 
            ret = -3;
            goto ERR_i2c_gpio_mem_write;
        }
    }
    else{
        thiz->send_byte(num, (MemAddress>>8) & 0xFF);
        if(thiz->wait_ack(num)){
            thiz->stop(num); 
            ret = -4;
            goto ERR_i2c_gpio_mem_write;
        }
        thiz->send_byte(num, MemAddress & 0xFF);
        if(thiz->wait_ack(num)){
            thiz->stop(num); 
            ret = -5;
            goto ERR_i2c_gpio_mem_write;
        }
    }

    // data
    while(Size--)
    {
      thiz->send_byte(num, *pData);
      thiz->wait_ack(num);
      pData++;
    }

    // stop
    thiz->stop(num);
    return 0;

ERR_i2c_gpio_mem_write:
    return ret;
}
int HAL_SOFT_I2C_MemRead(ae_iic_num num, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{
    as_iic_api *thiz = &iic_data[num].api;
    int ret = 0;

    thiz->init(num);
    thiz->stop(num);
    // start
    if(thiz->start(num)){
        ret = -1;
        goto ERR_i2c_gpio_mem_read;
    }

    // DeviceAddress Write
    thiz->send_byte(num, DevAddress & 0xFE);
    if(thiz->wait_ack(num)){
        thiz->stop(num); 
        ret = -2;
        goto ERR_i2c_gpio_mem_read;
    }

    // MemAddress
    if (I2C_ADDR_SIZE_8BIT == MemAddSize){
        thiz->send_byte(num, MemAddress & 0xFF);
        if(thiz->wait_ack(num)){
            thiz->stop(num); 
            ret = -3;
            goto ERR_i2c_gpio_mem_read;
        }
    }
    else{
        thiz->send_byte(num, (MemAddress>>8) & 0xFF);
        if(thiz->wait_ack(num)){
            thiz->stop(num); 
            ret = -4;
            goto ERR_i2c_gpio_mem_read;
        }
        thiz->send_byte(num, MemAddress & 0xFF);
        if(thiz->wait_ack(num)){
            thiz->stop(num); 
            ret = -5;
            goto ERR_i2c_gpio_mem_read;
        }
    }

    // start
    thiz->stop(num);
    thiz->start(num);

    // DeviceAddress Read
    thiz->send_byte(num, DevAddress | 0x01);
    if(thiz->wait_ack(num)){
        thiz->stop(num); 
        ret = -6;
        goto ERR_i2c_gpio_mem_read;
    }

    // data
    while(Size){
        *pData = thiz->read_byte(num);
        if (Size == 1)
            thiz->nack(num);
        else 
            thiz->ack(num);
        pData++;
        Size--;
    }

    // stop
    thiz->stop(num);
    return 0;

ERR_i2c_gpio_mem_read:
    return ret;
}
void HAL_SOFT_IIC_GPIO_Init(ae_iic_num num)
{    
    gpio_init(iic_data[num].sda_port, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, iic_data[num].sda_pin);
    gpio_init(iic_data[num].scl_port, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, iic_data[num].scl_pin);
    gpio_bit_write(iic_data[num].sda_port, iic_data[num].sda_pin, RESET);
    gpio_bit_write(iic_data[num].scl_port, iic_data[num].scl_pin, RESET);
    HAL_Delay(100);
}

void HAL_SOFT_I2C_Init(ae_iic_num num, uint32_t sda_port, uint32_t sda_pin, uint32_t scl_port, uint32_t scl_pin)
{
    as_iic_api *thiz = &iic_data[num].api;
    
    iic_data[num].sda_port = sda_port;
    iic_data[num].sda_pin  = sda_pin;
    iic_data[num].scl_port = scl_port;
    iic_data[num].scl_pin  = scl_pin;
    
    thiz->scl_0      = HAL_SOFT_I2C_Scl_0;
    thiz->scl_1      = HAL_SOFT_I2C_Scl_1;
    thiz->sda_0      = HAL_SOFT_I2C_Sda_0;
    thiz->sda_1      = HAL_SOFT_I2C_Sda_1;
    thiz->sda_read   = HAL_SOFT_I2C_Sda_Read;
    thiz->delay      = HAL_SOFT_I2C_Delay;
    thiz->init       = HAL_SOFT_IIC_GPIO_Init;

    thiz->start      = HAL_SOFT_I2C_Start;
    thiz->stop       = HAL_SOFT_I2C_Stop;
    thiz->ack        = HAL_SOFT_I2C_Ack;
    thiz->nack       = HAL_SOFT_I2C_Nack;
    thiz->wait_ack   = HAL_SOFT_I2C_WaitAck;
    thiz->send_byte  = HAL_SOFT_I2C_SendByte;
    thiz->read_byte  = HAL_SOFT_I2C_ReadByte;

    thiz->reset      = HAL_SOFT_I2C_Reset;
    thiz->transmit   = HAL_SOFT_I2C_Transmit;
    thiz->receive    = HAL_SOFT_I2C_Receive;
    thiz->mem_write  = HAL_SOFT_I2C_MemWrite;
    thiz->mem_read   = HAL_SOFT_I2C_MemRead;

    thiz->init(num);
}

