/*
 * @File: flash.h
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.FLASH drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:14:08
 * @Copyright: Copyright (c) 2020
 */
#ifndef __FLASH_H__
#define __FLASH_H__

#include "board.h"

#define FLASH_PAGE_SIZE 2048

void HAL_Flash_Erase(uint32_t addr, uint16_t nums);

void HAL_Flash_Write_HalfWord(uint32_t addr, uint16_t halfword);
void HAL_Flash_Write_Word(uint32_t addr, uint32_t word);

uint16_t HAL_Flash_Read_HalfWord(uint32_t addr);
uint32_t HAL_Flash_Read_Word(uint32_t addr);

void HAL_Flash_Write_Buff(uint32_t addr, uint8_t *buff, uint16_t nums);
void HAL_Flash_Read_Buff(uint32_t addr, uint8_t *buff, uint16_t nums);

#endif  /* __FLASH_H__ */
