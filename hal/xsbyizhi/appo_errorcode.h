#ifndef _APPO_ERROR_H_
#define _APPO_ERROR_H_
#include "board.h"
#define SYSTEM_ERR_MAX                (10)

typedef enum
{
	AIR_INTAKE_FAN1,        // 四线风扇
	AIR_INTAKE_FAN2,        // 四线风扇
	HeatSinkFan1,           // 四线风扇
	HeatSinkFan2,           // 四线风扇
	DmdFan1,                // 三线风扇
	CwFan1,                 // 三线风扇
	Fan7,                   // 电源
	FAN_MAX,
}FAN_NUM;

typedef enum
{
    APPO_ERROR_CODE_MainModel_SignalLinux = 0x1,
    APPO_ERROR_CODE_MainModel_SignalSTM32 = 0x2,
    APPO_ERROR_CODE_MainModel_OpticalMecSTM32=0x3,
    APPO_ERROR_CODE_MainModel_ESP32=0x4,
    APPO_ERROR_CODE_MainModel_IMB=0x5,
    APPO_ERROR_CODE_MainModel_Monitor=0x6,
    APPO_ERROR_CODE_MainModel_BARCO=0x7,
    APPO_ERROR_CODE_MainModel_WaterCooling=0x8,
    APPO_ERROR_CODE_MainModel_Power=0x9,
    APPO_ERROR_CODE_MainModel_DLP=0xa,
    APPO_ERROR_CODE_MainModel_FPGA=0xb,
    APPO_ERROR_CODE_MainModel_ST9320=0xc,
    APPO_ERROR_CODE_MainModel_V56=0xd,
    APPO_ERROR_CODE_MainModel_MAX
}e_mainMode ;


 typedef enum 
{
    
    APPO_ERROR_CODE_SubModel_None=0x0,
    APPO_ERROR_CODE_SubModel_SignalLinux=0x1,
    APPO_ERROR_CODE_SubModel_SignalSTM32=0x2,
    APPO_ERROR_CODE_SubModel_OpticalMecSTM32=0x3,
    APPO_ERROR_CODE_SubModel_ESP32=0x4,
    APPO_ERROR_CODE_SubModel_IMB=0x5,
    APPO_ERROR_CODE_SubModel_Monitor=0x6,
    APPO_ERROR_CODE_SubModel_BARCO=0x7,
    APPO_ERROR_CODE_SubModel_Water_Cooling=0x8,
    APPO_ERROR_CODE_SubModel_Power=0x9,
    APPO_ERROR_CODE_SubModel_DLP=0xa,
    APPO_ERROR_CODE_SubModel_FPGA=0xb,
    APPO_ERROR_CODE_SubModel_ST9320=0xc,
    APPO_ERROR_CODE_SubModel_V56=0xd,
    APPO_ERROR_CODE_SubModel_System=0xe,
    APPO_ERROR_CODE_SubModel_CW=0xf,
    APPO_ERROR_CODE_SubModel_Fan=0x10,
    APPO_ERROR_CODE_SubModel_Laser=0x11,
    APPO_ERROR_CODE_SubModel_WaterPump=0x12,
    APPO_ERROR_CODE_SubModel_Power1=0x13,
    APPO_ERROR_CODE_SubModel_Environment=0x14,
    APPO_ERROR_CODE_SubModel_CommPort=0x15,
    APPO_ERROR_CODE_SubModel_SerialPort=0x16,
    APPO_ERROR_CODE_SubModel_SystemIO=0x17,
    APPO_ERROR_CODE_SubModel_PrivilegedTime=0x18,
    APPO_ERROR_CODE_SubModel_LightSensor=0x19,
    APPO_ERROR_CODE_SubModel_XPR=0x1a,
    APPO_ERROR_CODE_SubModel_DMD=0x20,
    APPO_ERROR_CODE_SubModel_HumiditySensor=0x21,
    APPO_ERROR_CODE_SubModel_Chiller=0x22,
    APPO_ERROR_CODE_SubModel_Spacing=0x23,
    APPO_ERROR_CODE_SubModel_Threshold=0x24,
    APPO_ERROR_CODE_SubModel_CameraLens=0x25,
    APPO_ERROR_CODE_SubModel_Tec=0x26,
    APPO_ERROR_CODE_SubModel_FLASH=0x27,
	APPO_ERROR_CODE_SubModel_Memory=0x28,
	APPO_ERROR_CODE_SubModel_MainTask=0x29,
	APPO_ERROR_CODE_SubModel_Core=0x2a,
	APPO_ERROR_CODE_SubModel_ASIC=0x2b,
	APPO_ERROR_CODE_SubModel_SequencerConfig=0x2c,
	APPO_ERROR_CODE_SubModel_PollingTask=0x2d,
	APPO_ERROR_CODE_SubModel_Configure=0x2e,
	APPO_ERROR_CODE_SubModel_SystemMonitor=0x2f,
	APPO_ERROR_CODE_SubModel_DDPAsic=0x30,
	APPO_ERROR_CODE_SubModel_DMDAsic=0x31,
	APPO_ERROR_CODE_SubModel_Ulp=0x32,
	APPO_ERROR_CODE_SubModel_Segment_Fault=0x33,
	APPO_ERROR_CODE_SubModel_EEPROM=0x34,
	APPO_ERROR_CODE_SubModel_MemoryPool=0x35,
	APPO_ERROR_CODE_SubModel_AppConfigure=0x36,
	APPO_ERROR_CODE_SubModel_MAX	
}e_subModel;


 typedef enum
{   APPO_ERROR_CODE_ModelProperty_None=0x0,
    APPO_ERROR_CODE_ModelProperty_Init=0x1,
    APPO_ERROR_CODE_ModelProperty_Start=0x2,
    APPO_ERROR_CODE_ModelProperty_Close=0x3,
    APPO_ERROR_CODE_ModelProperty_Upgrade=0x4,
    APPO_ERROR_CODE_ModelProperty_Sync=0x5,
    APPO_ERROR_CODE_ModelProperty_Temp=0x6,
    APPO_ERROR_CODE_ModelProperty_Comm=0x7,
    APPO_ERROR_CODE_ModelProperty_Drive=0x8,
    APPO_ERROR_CODE_ModelProperty_Humidity=0x9,
    APPO_ERROR_CODE_ModelProperty_Voltage=0xa,
    APPO_ERROR_CODE_ModelProperty_Current=0xb,
    APPO_ERROR_CODE_ModelProperty_Flow=0xc,
    APPO_ERROR_CODE_ModelProperty_WaterTemp=0xd,
    APPO_ERROR_CODE_ModelProperty_WaterTempProt=0xe,
    APPO_ERROR_CODE_ModelProperty_HeatDissipationProtection=0xf,
    APPO_ERROR_CODE_ModelProperty_OnOff=0x10,
    APPO_ERROR_CODE_ModelProperty_WaterLevelProt=0x11,
    APPO_ERROR_CODE_ModelProperty_Rev=0x12,
    APPO_ERROR_CODE_ModelProperty_Type=0x13,
    APPO_ERROR_CODE_ModelProperty_MAX
} e_modelProperty;




 typedef enum {
    APPO_ERROR_CODE_ModelID_Zero=0,
    APPO_ERROR_CODE_ModelID_One=0x1,
    APPO_ERROR_CODE_ModelID_Two=0x2,
    APPO_ERROR_CODE_ModelID_Three=0x3,
    APPO_ERROR_CODE_ModelID_Four=0x4,
    APPO_ERROR_CODE_ModelID_Five=0x5,
    APPO_ERROR_CODE_ModelID_Six=0x6,
    APPO_ERROR_CODE_ModelID_Seven=0x7,
    APPO_ERROR_CODE_ModelID_Eight=0x8,
    APPO_ERROR_CODE_ModelID_Night=0x9,
    APPO_ERROR_CODE_ModelID_Ten=0xa,
    APPO_ERROR_CODE_ModelID_Eleven=0xb,
    APPO_ERROR_CODE_ModelID_Twelve=0xc,
    APPO_ERROR_CODE_ModelID_Thirteen=0xd,
    APPO_ERROR_CODE_ModelID_Fourteen=0xe,
    APPO_ERROR_CODE_ModelID_Fifteen=0xf,
    APPO_ERROR_CODE_ModelID_MAX
}e_modelId;



typedef enum
{
    
    APPO_ERROR_CODE_ModelState_Fail=0x0,
    APPO_ERROR_CODE_ModelState_Success=0x1,
    APPO_ERROR_CODE_ModelState_Overhigh=0x2,
    APPO_ERROR_CODE_ModelState_Overlow=0x3,
    APPO_ERROR_CODE_ModelState_Timeout=0x4,
    APPO_ERROR_CODE_ModelState_MAX
}e_modelState ;

typedef union
{
	uint32_t err_code;
	struct
	{
		uint8_t  model_status:4;
		uint8_t  model_id:4;
		uint8_t  model_property; 
		uint8_t  sub_model;
		uint8_t  main_model;
	}detail;

}struct_system_err;

/* Exported macro ------------------------------------------------------------*/
typedef enum
{
	SYS_STATUS_IDLE,
	SYS_STATUS_SELFCHECKING,
	SYS_STATUS_SELFCHECKOK,
	SYS_STATUS_LASER_ONING,
	SYS_STATUS_LASER_ON,
	SYS_STATUS_LASER_OFFING,
	SYS_STATUS_LASER_OFF,
	SYS_STATUS_ERROR,
	SYS_STATUS_SOFTWARE_UPDATE,

	MAX_SYS_STATUS
}enum_SysStauts;

/* Exported variables --------------------------------------------------------*/
typedef struct System
{

	uint8_t err_count;	// total error numbers
	uint8_t err_index;	// index of current error
	struct_system_err  err[SYSTEM_ERR_MAX];

	int(*system_err_set)(struct System *thiz,e_mainMode main_mode, e_subModel sub_model, e_modelProperty property, e_modelId mode_id, e_modelState state);
	int(*system_err_get)(struct System *thiz,uint8_t err_index,uint32_t  *err_code);

}Struct_System;
extern Struct_System  g_system;

int ErrorCode_Init(void);
#define errorcode(Main,Sub,Property,Id,State) ((Main<<24)||(Sub<<16)||(Property<<8)||(Id<<4)||(State))

#endif //_APPO_ERROR_H_
