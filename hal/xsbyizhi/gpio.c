/*
 * @File: gpio.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.GPIO drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-12-09 19:52:45
 * @Copyright: Copyright (c) 2020
 */
#include "gpio.h"
#define NAME "gpio"
void HAL_GPIO_Init(void)
{
    /* GPIO Ports Clock Enable */
    rcu_periph_clock_enable(RCU_GPIOA);
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_AF);
    // PA0 PA1 PA2 input
    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_6);
    gpio_exti_source_select(GPIO_PORT_SOURCE_GPIOA, GPIO_PIN_SOURCE_6);
    exti_init(EXTI_6, EXTI_INTERRUPT, EXTI_TRIG_BOTH);
    nvic_irq_enable(EXTI5_9_IRQn, 2, 0);
    exti_interrupt_flag_clear(EXTI_6);
	
	
    // PB10 PB11 PA15 output 0
    gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10);
    gpio_bit_set(GPIOB, GPIO_PIN_10);
	gpio_bit_reset(GPIOB, GPIO_PIN_10);

}

void EXTI5_9_IRQHandler(void)
{

    if (RESET != exti_interrupt_flag_get(EXTI_6)){

		LOG(INFO, "EXTI5_9_IRQHandler\r\n ");
    }
	exti_interrupt_flag_clear(EXTI_6);
}



