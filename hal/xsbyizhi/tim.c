/*
 * @File: tim.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.TIM drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-26 13:57:02
 * @Copyright: Copyright (c) 2020
 */

#include "tim.h"
#define NAME "timer"
static timer_parameter_struct timer0_initpara;
static timer_parameter_struct timer1_initpara;
static timer_parameter_struct timer2_initpara;
static timer_parameter_struct timer3_initpara;

void HAL_TIM0_Init(void)
{
    timer_oc_parameter_struct timer_ocintpara;

    rcu_periph_clock_enable(RCU_TIMER0);
    timer_deinit(TIMER0);

    // 12k
    /* TIMER0 configuration */
    timer0_initpara.prescaler         = (SystemCoreClock/12000000) - 1;     // 12M
    timer0_initpara.alignedmode       = TIMER_COUNTER_EDGE;
    timer0_initpara.counterdirection  = TIMER_COUNTER_UP;
    timer0_initpara.period            = 1000 - 1;
    timer0_initpara.clockdivision     = TIMER_CKDIV_DIV1;
    timer0_initpara.repetitioncounter = 0;
    timer_init(TIMER0, &timer0_initpara);

    /* CH0 configuration in PWM mode0 */
    timer_ocintpara.outputstate  = TIMER_CCX_DISABLE;
    timer_ocintpara.outputnstate = TIMER_CCXN_ENABLE;
    timer_ocintpara.ocpolarity   = TIMER_OC_POLARITY_HIGH;
    timer_ocintpara.ocnpolarity  = TIMER_OCN_POLARITY_HIGH;
    timer_ocintpara.ocidlestate  = TIMER_OC_IDLE_STATE_LOW;
    timer_ocintpara.ocnidlestate = TIMER_OCN_IDLE_STATE_LOW;
    timer_channel_output_config(TIMER0, TIMER_CH_0, &timer_ocintpara);

    timer_channel_output_pulse_value_config(TIMER0, TIMER_CH_0, 0);
    timer_channel_output_mode_config(TIMER0, TIMER_CH_0, TIMER_OC_MODE_PWM0);
    timer_channel_output_shadow_config(TIMER0, TIMER_CH_0, TIMER_OC_SHADOW_DISABLE);
	
	/*这里需要强调一点的是,如果你使用TIMER0产生PWM的时候必须加上*/
	/*timer_primary_output_config(TIMER0,ENABLE);*/
	/*否则无法输出PWM!!!注意!*/
    /* TIMER0 primary output enable */
//    timer_primary_output_config(TIMER0, ENABLE);
    /* auto-reload preload enable */
    timer_auto_reload_shadow_enable(TIMER0); 
    
    /* enable TIMER0 */
    timer_enable(TIMER0);
}

void HAL_TIM1_Init(void)
{
    rcu_periph_clock_enable(RCU_TIMER1);
    timer_deinit(TIMER1);

    /* TIMER1 configuration */
    timer1_initpara.prescaler = (SystemCoreClock*1) - 1;//1:1ms   1000:1us
    timer1_initpara.alignedmode = TIMER_COUNTER_EDGE;
    timer1_initpara.counterdirection = TIMER_COUNTER_UP;
    timer1_initpara.period = 1000 - 1;
    timer1_initpara.clockdivision = TIMER_CKDIV_DIV1;
    timer1_initpara.repetitioncounter = 0;
    timer_init(TIMER1, &timer1_initpara);

    timer_auto_reload_shadow_enable(TIMER1);
    timer_interrupt_enable(TIMER1, TIMER_INT_UP);
    timer_enable(TIMER1);
    nvic_irq_enable(TIMER1_IRQn, 0, 0);
}

void HAL_TIM2_Init(void)
{
    rcu_periph_clock_enable(RCU_TIMER2);
    timer_deinit(TIMER2);

    /* TIMER0 configuration */
    timer2_initpara.prescaler = ((SystemCoreClock/1) / SYS_TIMER_PPMS) - 1;
    timer2_initpara.alignedmode = TIMER_COUNTER_EDGE;
    timer2_initpara.counterdirection = TIMER_COUNTER_UP;
    timer2_initpara.period = 0xFFFF;
    timer2_initpara.clockdivision = TIMER_CKDIV_DIV1;
    timer2_initpara.repetitioncounter = 0;
    timer_init(TIMER2, &timer2_initpara);

    timer_auto_reload_shadow_enable(TIMER2);
    timer_interrupt_enable(TIMER2, TIMER_INT_UP);
    timer_enable(TIMER2);
    nvic_irq_enable(TIMER2_IRQn, 0, 0);
}

void HAL_TIM3_Init(void)
{
    rcu_periph_clock_enable(RCU_TIMER3);
    timer_deinit(TIMER3);

    /* TIMER3 configuration */
    timer3_initpara.prescaler = (SystemCoreClock/1) - 1;
    timer3_initpara.alignedmode = TIMER_COUNTER_EDGE;
    timer3_initpara.counterdirection = TIMER_COUNTER_UP;
    timer3_initpara.period = 1000 - 1;
    timer3_initpara.clockdivision = TIMER_CKDIV_DIV1;
    timer3_initpara.repetitioncounter = 0;
    timer_init(TIMER3, &timer3_initpara);

    timer_auto_reload_shadow_enable(TIMER3);
    timer_interrupt_enable(TIMER3, TIMER_INT_UP);
    timer_enable(TIMER3);
    nvic_irq_enable(TIMER3_IRQn, 0, 0);
}

void HAL_TIM_Stop(ae_tim_num num)
{
    switch(num){
        case TIM_NUM1:
            timer_disable(TIMER1);
            break;
        case TIM_NUM2:
            timer_disable(TIMER2);
            break;
        case TIM_NUM3:
            timer_disable(TIMER3);
            break;
    }
}


void TIMER0_IRQHandler(void)
{
    if (RESET != timer_interrupt_flag_get(TIMER0, TIMER_INT_FLAG_UP))
    {
        timer_interrupt_flag_clear(TIMER0, TIMER_INT_FLAG_UP);
        //HAL_TIM_UP_IRQHandler(TIM_NUM0);
    }
}

void TIMER1_IRQHandler(void)
{

    if (RESET != timer_interrupt_flag_get(TIMER1, TIMER_INT_FLAG_UP))
    {
		timer_interrupt_flag_clear(TIMER1, TIMER_INT_FLAG_UP);
		LOG(INFO, "TIMER1_IRQHandler\r\n ");
    }
}

void TIMER2_IRQHandler(void)
{
    if (RESET != timer_interrupt_flag_get(TIMER2, TIMER_INT_FLAG_UP))
    {
        timer_interrupt_flag_clear(TIMER2, TIMER_INT_FLAG_UP);
        //HAL_TIM_UP_IRQHandler(TIM_NUM2);
    }
}

void TIMER3_IRQHandler(void)
{
    if (RESET != timer_interrupt_flag_get(TIMER3, TIMER_INT_FLAG_UP))
    {
        timer_interrupt_flag_clear(TIMER3, TIMER_INT_FLAG_UP);
        LOG(INFO, "TIMER3_IRQHandler\r\n ");
    }
}

