/*
 * @File: tim.h
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.TIM drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:14:24
 * @Copyright: Copyright (c) 2020
 */

#ifndef __TIM_H__
#define __TIM_H__
#ifdef __cplusplus
extern "C"
{
#endif

#include "board.h"

typedef enum
{
    TIM_NUM0 = 0,
    TIM_NUM1,
    TIM_NUM2,
    TIM_NUM3,
} ae_tim_num;


void HAL_TIM0_Init(void);
void HAL_TIM1_Init(void);
void HAL_TIM2_Init(void);
void HAL_TIM3_Init(void);

void HAL_TIM_Stop(ae_tim_num num);

void TIMER1_IRQHandler(void);

#ifdef __cplusplus
}
#endif
#endif /* __TIM_H__ */
