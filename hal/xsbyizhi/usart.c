/* USART0串口初始化函数
 * 参数：波特率
 * 返回值：无	*/
#include "usart.h"

uint8_t USART_RX_BUF[USART_REC_LEN];     //接收缓冲,最大USART_REC_LEN个字节.

/****************************支持Printf****************************/
/* 加入以下代码,支持printf函数,而不需要选择use MicroLIB	*/  
#if 1
#pragma import(__use_no_semihosting)             
/* 标准库需要的支持函数 */                
struct __FILE 
{ 
	int handle; 
}; 

FILE __stdout;       
/* 定义_sys_exit()以避免使用半主机模式  */  
void _sys_exit(int x) 
{ 
	x = x; 
} 
/* 重定义fputc函数 */
int fputc(int ch, FILE *f)
{   
	while (RESET == usart_flag_get(USART0, USART_FLAG_TBE));	
	usart_data_transmit(USART0, (uint8_t)ch);
	return ch;
}
#endif 

/****************************串口操作****************************/
void HAL_UART0_Init(void)
{	
	/* 使能 GPIOA 时钟 */
    rcu_periph_clock_enable(RCU_GPIOA);
	
    /* 使能 USART0 时钟 */
    rcu_periph_clock_enable(RCU_USART0);

    /* PA9  复用为 USART0_Tx */
    gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ,GPIO_PIN_9);

    /* PA10 复用为 USARTx_Rx */
    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ,GPIO_PIN_10);
					
    /* USART0 初始化配置 */
    usart_deinit(USART0);
    usart_baudrate_set(USART0, 115200U);						/* 设置波特率 */
    usart_word_length_set(USART0, USART_WL_8BIT);
    usart_stop_bit_set(USART0, USART_STB_1BIT);
    usart_parity_config(USART0, USART_PM_NONE);
    usart_hardware_flow_rts_config(USART0, USART_RTS_DISABLE);
    usart_hardware_flow_cts_config(USART0, USART_CTS_DISABLE);
    usart_receive_config(USART0, USART_RECEIVE_ENABLE);
    usart_transmit_config(USART0, USART_TRANSMIT_ENABLE);
	usart_enable(USART0);
	
	usart_interrupt_enable(USART0, USART_INT_RBNE);
    nvic_irq_enable(USART0_IRQn, 0, 0);
}

void HAL_UART1_Init(void)
{
    /* enable GPIO clock */
    rcu_periph_clock_enable(RCU_GPIOA);

    /* enable USART clock */
    rcu_periph_clock_enable(RCU_USART1);

    /* connect port to USARTx_Tx */
    gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_2);

    /* connect port to USARTx_Rx */
    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_3);

    /* USART configure */
    usart_deinit(USART1);
    usart_baudrate_set(USART1, 115200U);
    usart_word_length_set(USART1, USART_WL_8BIT);
    usart_stop_bit_set(USART1, USART_STB_1BIT);
    usart_parity_config(USART1, USART_PM_NONE);
    usart_hardware_flow_rts_config(USART1, USART_RTS_DISABLE);
    usart_hardware_flow_cts_config(USART1, USART_CTS_DISABLE);
    usart_receive_config(USART1, USART_RECEIVE_ENABLE);
    usart_transmit_config(USART1, USART_TRANSMIT_ENABLE);
    usart_enable(USART1);

    usart_interrupt_enable(USART1, USART_INT_RBNE);
    nvic_irq_enable(USART1_IRQn, 0, 0);
}

void HAL_UART2_Init(void)
{
    /* enable GPIO clock */
    rcu_periph_clock_enable(RCU_GPIOB);

    /* enable USART clock */
    rcu_periph_clock_enable(RCU_USART2);

    /* connect port to USARTx_Tx */
    gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_10);

    /* connect port to USARTx_Rx */
    gpio_init(GPIOB, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ, GPIO_PIN_11);

    /* USART configure */
    usart_deinit(USART2);
    usart_baudrate_set(USART2, 115200U);
    usart_word_length_set(USART2, USART_WL_8BIT);
    usart_stop_bit_set(USART2, USART_STB_1BIT);
    usart_parity_config(USART2, USART_PM_NONE);
    usart_hardware_flow_rts_config(USART2, USART_RTS_DISABLE);
    usart_hardware_flow_cts_config(USART2, USART_CTS_DISABLE);
    usart_receive_config(USART2, USART_RECEIVE_ENABLE);
    usart_transmit_config(USART2, USART_TRANSMIT_ENABLE);
    usart_enable(USART2);

    usart_interrupt_enable(USART2, USART_INT_RBNE);
    nvic_irq_enable(USART2_IRQn, 0, 0);
}

void HAL_UART_SendByte(ae_uart_num num, uint8_t dat)
{
    uint32_t port;
    uint32_t tick;

    switch(SYS_COMM_UART){
        case UART_NUM0:
            port = USART0;
            break;
        case UART_NUM1:
            port = USART1;
            break;
        case UART_NUM2:
            port = USART2;
            break;
    }
    tick = millis();
    while( (RESET==usart_flag_get(port, USART_FLAG_TBE)) && (millis_elapsed(tick)<10) );
    usart_data_transmit(port, (uint8_t)dat);
}

void USART0_IRQHandler(void)
{
	int index = 0;
	uint8_t tmp;
    if (RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_RBNE)){
		usart_interrupt_flag_clear(USART2,USART_INT_FLAG_RBNE);
        tmp = (uint8_t)usart_data_receive(USART0);
		USART_RX_BUF[index] = tmp;
		index ++;
    }
    usart_interrupt_flag_clear(USART0, USART_INT_FLAG_PERR);
    usart_interrupt_flag_clear(USART0, USART_INT_FLAG_RBNE_ORERR);
    usart_interrupt_flag_clear(USART0, USART_INT_FLAG_ERR_ORERR);
    usart_interrupt_flag_clear(USART0, USART_INT_FLAG_ERR_NERR);
    usart_interrupt_flag_clear(USART0, USART_INT_FLAG_ERR_FERR);
}

