/*
 * @File: log.h
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: Format the log print
 * @version: v1.0.0
 * @Date: 2021-06-19 11:32:34
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 09:54:34
 * @Copyright: Copyright (c) 2020
 */

#ifndef __LOG_H
#define __LOG_H
#include "board.h"

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

    typedef enum
    {
        QUIET = 0,
        ERR,
        WARN,
        INFO,
        DEBUG,
    } ae_log_level;

#define LOG(l, x...)   { log_puts(l, NAME, x);}

extern const char *log_level_str[];
void log_set_level(ae_log_level level);
ae_log_level log_get_level(void);
int log_puts(ae_log_level level, const char *module, char *fmt, ...);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
