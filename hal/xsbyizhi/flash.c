/*
 * @File: flash.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.FLASH drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:14:05
 * @Copyright: Copyright (c) 2020
 */
#include "flash.h"

void HAL_Flash_Erase(uint32_t addr, uint16_t nums)
{
    int i;

    fmc_unlock();
    for (i = 0; i < nums; i++)
    {
        fmc_page_erase(addr + FLASH_PAGE_SIZE * i);
    }
    fmc_lock();
}

void HAL_Flash_Write_HalfWord(uint32_t addr, uint16_t halfword)
{
    fmc_unlock();
    fmc_halfword_program(addr, halfword);
    fmc_lock();
}

void HAL_Flash_Write_Word(uint32_t addr, uint32_t word)
{
    fmc_unlock();
    fmc_word_program(addr, word);
    fmc_lock();
}

uint16_t HAL_Flash_Read_HalfWord(uint32_t addr)
{
    uint16_t halfword;

    halfword = *(__IO uint16_t *)(addr);

    return halfword;
}

uint32_t HAL_Flash_Read_Word(uint32_t addr)
{
    uint32_t word;

    word = *(__IO uint32_t *)(addr);

    return word;
}

void HAL_Flash_Write_Buff(uint32_t addr, uint8_t *buff, uint16_t nums)
{
    int i;
    uint32_t tmp = 0;

    fmc_unlock();
    for (i = 0; i < nums; i+=4)
    {
        tmp = buff[i] << 0;
        tmp += buff[i+1] << 8;
        tmp += buff[i+2] << 16;
        tmp += buff[i+3] << 24;
        fmc_word_program(addr + i, tmp);
    }
    tmp = 0;
    switch(nums%4){
        case 1:
            tmp += buff[nums-1] << 24;
            break;
        case 2:
            tmp += buff[nums-1] << 24;
            tmp += buff[nums-2] << 16;
            break;
        case 3:
            tmp += buff[nums-1] << 24;
            tmp += buff[nums-2] << 16;
            tmp += buff[nums-3] << 8;
            break;
    }
    if( nums%4 != 0 ){
        fmc_word_program(addr + i, tmp);
    }
    fmc_lock();
}

void HAL_Flash_Read_Buff(uint32_t addr, uint8_t *buff, uint16_t nums)
{
    int i;

    fmc_unlock();
    for (i = 0; i < nums; i++)
    {
        buff[i] = *(__IO uint8_t *)(addr + i);
    }
    fmc_lock();
}
