/*
 * @File: board.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.Board level resource initialization
 * @version: v1.0.0
 * @Date: 2021-06-05 14:39:47
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-10-05 15:04:54
 * @Copyright: Copyright (c) 2020
 */

#include "board.h"
uint32_t sys_tick_cnt = 0;
static uint16_t sys_ustick_cnt = 0;

/*!
    \brief      configure systick
    \param[in]  none
    \param[out] none
    \retval     none
*/
void systick_config(void)
{
    /* setup systick timer for 1000Hz interrupts */
    if (SysTick_Config(SystemCoreClock / 1000U)){
        /* capture error */
        while (1){
        }
    }
    /* configure the systick handler priority */
    NVIC_SetPriority(SysTick_IRQn, 0x00U);
}

void HAL_Soft_Reset(void)
{
    __disable_irq();
    NVIC_SystemReset();
}

void HAL_TIM_UsTick_Inc(void)
{
    sys_ustick_cnt ++;
}

void HAL_Board_Init(void)
{
    // RCC and system tick
    SystemCoreClockUpdate();
    systick_config();
	ErrorCode_Init();
	
	HAL_GPIO_Init();
	
    HAL_TIM0_Init();
    HAL_TIM1_Init();
    HAL_TIM2_Init();
    HAL_TIM3_Init();

    HAL_UART0_Init();
    HAL_UART1_Init();
    HAL_UART2_Init();

	LED_INIT();
}

uint32_t nanos(void)
{
    uint32_t nos;

    switch(SYS_TICK_TIM){
        case TIM_NUM0:
            nos = (timer_counter_read(TIMER0) + sys_ustick_cnt * 0x10000);
            break;
        case TIM_NUM2:
            nos = (timer_counter_read(TIMER2) + sys_ustick_cnt * 0x10000);
            break;
    }

    return nos;
}

uint32_t nanos_elapsed(uint32_t n)
{
    return abs(nanos() - n);
}

uint32_t micros(void)
{
    return (nanos()) / SYS_TIMER_PPMS;
}

uint32_t micros_elapsed(uint32_t n)
{
    return abs(micros() - n);
}

uint32_t millis(void)
{
    return sys_tick_cnt;
}

uint32_t millis_elapsed(uint32_t n)
{
    return abs(millis() - n);
}


void HAL_Delay_us(uint32_t tus)
{
    uint32_t tick = micros();

    while( micros_elapsed(tick) <= tus );
}

void HAL_Delay_ms(uint32_t tms)
{
    uint32_t tick = sys_tick_cnt;

    while( abs(sys_tick_cnt - tick) <= tms );
}

