/*
 * @File: adc.h
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.ADC drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:13:58
 * @Copyright: Copyright (c) 2020
 */


#ifndef __ADC_H__
#define __ADC_H__
#ifdef __cplusplus
extern "C"
{
#endif

#include "board.h"

    void HAL_ADC_Init(void);
    uint16_t HAL_ADC_GetAvg(uint8_t times);

#ifdef __cplusplus
}
#endif
#endif /* __ADC_H__ */
