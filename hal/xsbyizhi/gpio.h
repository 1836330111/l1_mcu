/*
 * @File: gpio.h
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.GPIO drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:14:13
 * @Copyright: Copyright (c) 2020
 */

#ifndef __GPIO_H__
#define __GPIO_H__
#ifdef __cplusplus
extern "C"
{
#endif

#include "board.h"

    typedef enum
    {
        GPIO_PIN0 = 0,
        GPIO_PIN1,
        GPIO_PIN2,
        GPIO_PIN3,
        GPIO_PIN4,
        GPIO_PIN5,
        GPIO_PIN6,
        GPIO_PIN7,
        GPIO_PIN8,
        GPIO_PIN9,
        GPIO_PIN10,
        GPIO_PIN11,
        GPIO_PIN12,
        GPIO_PIN13,
        GPIO_PIN14,
        GPIO_PIN15,
    } ae_Gpio_Pin;


void HAL_GPIO_Init(void);


#ifdef __cplusplus
}
#endif
#endif /* __GPIO_H__ */
