/*
 * @File: adc.c
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.ADC drive interface
 * @version: v1.0.0
 * @Date: 2021-06-05 14:16:08
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-09-13 11:13:55
 * @Copyright: Copyright (c) 2020
 */

#include "adc.h"


/* ADC init function */
void HAL_ADC_Init(void)
{
    gpio_init(GPIOA, GPIO_MODE_AIN, GPIO_OSPEED_50MHZ, GPIO_PIN_4);

    /* enable ADC clock */
    rcu_periph_clock_enable(RCU_ADC0);
    /* config ADC clock */
    rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV6);    // MAX 12M

    adc_deinit(ADC0);
    adc_data_alignment_config(ADC0, ADC_DATAALIGN_RIGHT);
    adc_channel_length_config(ADC0, ADC_REGULAR_CHANNEL, 1);
    adc_regular_channel_config(ADC0, 0, ADC_CHANNEL_4, ADC_SAMPLETIME_239POINT5);
    adc_interrupt_enable(ADC0, ADC_INT_EOC);
    adc_software_trigger_enable(ADC0, ADC_REGULAR_CHANNEL);

    /* enable ADC interface */
    adc_enable(ADC0);
    HAL_Delay(1);    
    /* ADC calibration and reset calibration */
    adc_calibration_enable(ADC0);
}

uint16_t HAL_ADC_GetAvg(uint8_t times)
{
    uint8_t t;
    uint32_t temp_val = 0;
    uint32_t tick;

    for (t = 0; t < times; t++)
    {
        adc_software_trigger_enable(ADC0, ADC_REGULAR_CHANNEL);
        tick = millis();
        while( (adc_interrupt_flag_get(ADC0, ADC_INT_FLAG_EOC)==RESET) && (millis_elapsed(tick)<1000) );
        temp_val += adc_regular_data_read(ADC0);
        HAL_Delay(1);
    }

    return temp_val / times;
}
