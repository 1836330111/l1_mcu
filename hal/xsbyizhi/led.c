/* LED的硬件初始化代码 */
#include "led.h"
#define NAME    "LED"

/* LED硬件初始化 */
void LED_INIT(void)
{
	/* 使能LED所使用的GPIOC时钟 */
    rcu_periph_clock_enable(RCU_GPIOC);

    /* 配置LED使用的引脚为推挽输出 */ 
    gpio_init(GPIOC, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_13);
    /* reset LED GPIO pin */
    gpio_bit_reset(GPIOC, GPIO_PIN_13);
}
/* 开启LED */
void LED_ON(void)
{
	gpio_bit_reset(GPIOC, GPIO_PIN_13);
    LOG(DEBUG, "LED_ON\r\n ");
}
/* 关闭LED */
void LED_OFF(void)
{
	gpio_bit_set(GPIOC, GPIO_PIN_13);
}
