/*
 * @File: board.h
 * @Author: xusongbin@appotronics.cn
 * @Descripttion: GD32F3xx.Board level resource initialization
 * @version: v1.0.0
 * @Date: 2021-06-05 14:39:47
 * @LastEditors: xusongbin@appotronics.cn
 * @LastEditTime: 2021-12-17 14:53:42
 * @Copyright: Copyright (c) 2020
 */

// Use for L055/L065/L032

#ifndef __BOARD_H__
#define __BOARD_H__

#ifdef __cplusplus
extern "C"
{
#endif
/* ---------------------------Includes -----------------------------*/
/* ----------C标准库 ---------*/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

/* ----------STM32标准库 ---------*/
#include "gd32f30x_libopt.h"
#include "gd32f30x_it.h"
#include "gd32f30x.h"

/* ----------外设 ---------*/
//#include "adc.h"
//#include "flash.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "i2c.h"
#include "led.h"
#include "log.h"
//#include "rtc.h"

/* ----------错误码 ---------*/
#include "appo_errorcode.h"

// system
#define SYS_COMM_UART           UART_NUM0
#define SYS_TICK_TIM            TIM_NUM2
#define SYS_SOFT_TIM            TIM_NUM1

#define SYS_IRQ_PORT            GPIOB
#define SYS_IRQ_PIN             GPIO_PIN_2
#define SYS_IRQ_SET(n)          {gpio_bit_write(SYS_IRQ_PORT, SYS_IRQ_PIN, n?SET:RESET);}

#define SYS_TIMER_PPMS          (2)     // MHz



extern uint32_t sys_tick_cnt;

void HAL_Soft_Reset(void);
void HAL_Board_Init(void);

void HAL_Delay_us(uint32_t tus);
void HAL_Delay_ms(uint32_t tms);

uint32_t nanos(void);
uint32_t nanos_elapsed(uint32_t n);

uint32_t micros(void);
uint32_t micros_elapsed(uint32_t n);

uint32_t millis(void);
uint32_t millis_elapsed(uint32_t n);


#ifdef __cplusplus
}
#endif

#endif /* __BOARD_H__ */
