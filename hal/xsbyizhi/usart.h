#ifndef __USART_H
#define __USART_H

#include "board.h"

#define USART_REC_LEN  			200  	//定义最大接收字节数 200
extern uint8_t  USART_RX_BUF[USART_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
typedef enum
    {
        UART_NUM0 = 0,
        UART_NUM1,
        UART_NUM2,
    } ae_uart_num;

/* 串口0初始化，参数为波特率 */
void HAL_UART0_Init(void);
void HAL_UART1_Init(void);
void HAL_UART2_Init(void);
	
void puthbuf(uint8_t *buf, int len);
void HAL_UART_SendByte(ae_uart_num num, uint8_t dat);
void USART0_IRQHandler(void);
	
#endif
	
